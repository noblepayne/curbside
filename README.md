# curbside

Simple blocking clojure solution for https://challenge.curbside.com

## Installation

Install [lein](https://leiningen.org/) and clone this repo.

## Usage

### Development
    $ lein repl
### Standalone
    $ lein run

## License

Copyright © 2018 Wesley Payne

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
