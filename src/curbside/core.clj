(ns curbside.core
  (:require [clj-http.client :as client]
            [cheshire.core :as json]
            [clojure.core.async :as async]
            [clojure.string :as str]
            [clojure.set :as set]))

;; static config
(def base-url "https://challenge.curbside.com/")

;; helper fns
(defn- keys->lowercase
  "Convert all keys in map to lowercase keywords"
  [input-map]
  (let [map-keys  (keys input-map)
        new-keys  (for [k map-keys]
                    [k (-> k name str/lower-case keyword)])]
    (set/rename-keys input-map (into {} new-keys))))

(defn- get-page
  "Return response body+headers as map.
  url is relative to base-url."
  [session url]
  (let [real-url (str base-url url)
        data     (client/get real-url
                             {:headers {"Session" session}})
        body     (-> data :body (json/parse-string true) keys->lowercase)
        headers  (:headers data)]
    (assoc body :headers headers)))

(defn- get-new-session
  "Retrieve new session id."
  []
  (-> (get-page nil "get-session")
      :session))

;; thread fns
(defn- worker
  "worker thread fn to make blocking http request.
  On response stores data in datastore atom. Retries on 429."
  [session url out-chan datastore]
  (try
    (let [{:keys [:id :next :secret] :as data} (get-page session url)]
      (swap! datastore assoc id data)
      (when next
        (run! #(async/>!! out-chan %) (flatten [next]))))
    (catch Exception e
      (let [data (ex-data e)
            status (:status data)]
        (when (= status 429)
          (async/>!! out-chan url))))))

(defn- dispatcher
  "Dispatching thread fn. Pulls urls to request from input channel
  and dispatches new worker threads. 10s timeout for new work items."
  [session in-chan datastore]
  (loop []
    (let [[node _] (async/alts!! [(async/timeout 10000) in-chan])]
      (when node
        (async/thread (worker session node in-chan datastore))
        (Thread/sleep 8) ;; hacky maximum rate limit ~125/s
        (recur)))))

;; high level control fns
(defn collect-data!
  "Start new session, load starting points, and run dispatcher thread."
  []
  (let [datastore (atom {})
        node-chan (async/chan 20000)
        session (get-new-session)
        starting-data (get-page session "start")
        starting-nodes (flatten [(:next starting-data)])]
    (println "Session:" session)
    (swap! datastore assoc "start" starting-data)              ;; store starting node
    (run! #(async/>!! node-chan %) starting-nodes)             ;; put starting nodes in queue
    (async/<!!                                                 ;; launch dispatcher and wait
     (async/thread (dispatcher session node-chan datastore)))
    (async/close! node-chan)
    @datastore))

(defn find-secrets
  "Depth first traversal of collected data for non-empty secrets."
  ([data] (find-secrets data [] '("start")))
  ([data secrets [cur & to-check]]
    (if cur
      (let [{:keys [:next :secret] :as node} (get data cur)
            new-secrets (if (not-empty secret)
                          (conj secrets secret)
                          secrets)
            new-next    (if next
                          (into to-check (reverse (flatten [next])))
                          to-check)]
        (recur data new-secrets new-next))
      (apply str secrets))))

;; main app
(defn -main
  "Collect data and determine secret."
  [& args]
  (-> (collect-data!) find-secrets println))
